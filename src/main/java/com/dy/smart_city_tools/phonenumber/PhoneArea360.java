package com.dy.smart_city_tools.phonenumber;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONObject;
import org.springframework.stereotype.Component;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.net.URL;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * 查询手机号运营商及归属地工具类
 *
 * @author 黑小虎（花名）
 * @date 2019/12/23 23:15
 */
@Component
public class PhoneArea360 {
    /**
     * JAVA正则表达式：验证手机号
     */
    private static String PHONE_NUMBER_REG = "^(13[0-9]|14[579]|15[0-3,5-9]|16[6]|17[0135678]|18[0-9]|19[89])\\d{8}$";

    /**
     * 判断手机号格式是否合法
     *
     * @param phoneNumber
     * @return String
     */
    public static String ifPhoneNumber(String phoneNumber) {
        if (phoneNumber == null)
            return "手机号不能为空";
        phoneNumber = Pattern.compile("[^0-9]")
                .matcher(phoneNumber)
                .replaceAll("")
                .trim();
        if (phoneNumber.length() != 11)
            return "手机号不是11位";
        Matcher matcher = Pattern.compile(PHONE_NUMBER_REG)
                .matcher(phoneNumber);
        if (!matcher.find())
            return "手机号不合法";
        return phoneNumber;
    }

    /**
     * 查询手机号运营商及归属地
     *
     * @param phoneNumber
     * @return PhoneAreaBean
     */
    public static PhoneAreaBean getPhoneAreaBean(String phoneNumber) {
        try {
            URL url = new URL("https://cx.shouji.360.cn/phonearea.php?number=" + phoneNumber);
            InputStream is = url.openConnection().getInputStream();
            ByteArrayOutputStream os = new ByteArrayOutputStream();
            byte[] b = new byte[1024];
            int len;
            while ((len = is.read(b)) != -1) os.write(b, 0, len);
            String respBody = os.toString("UTF-8");
            os.close();
            is.close();
            JSONObject jsonObject = JSON.parseObject(respBody);
            Object data = jsonObject.get("data");
            return JSON.parseObject(data.toString(), PhoneAreaBean.class);
        } catch (IOException e) {
            e.printStackTrace();
            return null;
        }
    }
}
