package com.dy.smart_city_tools.phonenumber;

import org.springframework.stereotype.Component;

import java.io.Serializable;

/**
 * 手机号运营商及归属地实体类
 *
 * @author 黑小虎（花名）
 * @date 2019/12/23 23:15
 */
@Component
public class PhoneAreaBean implements Serializable {
    private String province;
    private String city;
    private String sp;

    public String getProvince() {
        return province;
    }

    public void setProvince(String province) {
        this.province = province;
    }

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public String getSp() {
        return sp;
    }

    public void setSp(String sp) {
        this.sp = sp;
    }

    @Override
    public String toString() {
        return "PhoneAreaBean{" +
                "province='" + province + '\'' +
                ", city='" + city + '\'' +
                ", sp='" + sp + '\'' +
                '}';
    }
}
