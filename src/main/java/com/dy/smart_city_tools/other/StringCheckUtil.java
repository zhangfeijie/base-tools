package com.dy.smart_city_tools.other;

import java.util.regex.Pattern;

/**
 * StringCheckUtil
 *
 * @author 黑小虎（花名）
 * @date 2020/1/8 10:06
 */
public class StringCheckUtil {
    /**
     * 正则表达式：数字型
     */
    public static final String REGEX_NUMBER = "^[-\\+]?[\\d]*$";

    /**
     * 正则表达式：用户名
     */
    public static final String REGEX_USERNAME = "^[a-zA-Z0-9]\\w{5,20}$";

    /**
     * 正则表达式：手机号
     */
    public static final String REGEX_MOBILE = "^((17[0-9])|(14[0-9])|(13[0-9])|(15[^4,\\D])|(18[0-9]))\\d{8}$";

    /**
     * 正则表达式：邮箱号
     */
    public static final String REGEX_EMAIL = "^([a-z0-9A-Z]+[-|\\.]?)+[a-z0-9A-Z]@([a-z0-9A-Z]+(-[a-z0-9A-Z]+)?\\.)+[a-zA-Z]{2,}$";


    /**
     * 验证字符串是否为：数字型
     *
     * @param number
     * @return boolean（true 是，false 否）
     */
    public static boolean isNumber(String number) {
        return Pattern.matches(REGEX_NUMBER, number);
    }

    /**
     * 验证字符串是否为：用户名
     *
     * @param username
     * @return boolean（true 是，false 否）
     */
    public static boolean isUsername(String username) {
        return Pattern.matches(REGEX_USERNAME, username);
    }

    /**
     * 验证字符串是否为：手机号
     *
     * @param mobile
     * @return boolean（true 是，false 否）
     */
    public static boolean isMobile(String mobile) {
        return Pattern.matches(REGEX_MOBILE, mobile);
    }

    /**
     * 验证字符串是否为：邮箱号
     *
     * @param email
     * @return boolean（true 是，false 否）
     */
    public static boolean isEmail(String email) {
        return Pattern.matches(REGEX_EMAIL, email);
    }
}
