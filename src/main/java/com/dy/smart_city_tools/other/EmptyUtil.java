package com.dy.smart_city_tools.other;

import java.lang.reflect.Array;
import java.util.Collection;
import java.util.Map;

/**
 * EmptyUtil
 *
 * @author 黑小虎（花名）
 * @date 2019/12/29 15:44
 */
public class EmptyUtil {
    private EmptyUtil() {
        throw new UnsupportedOperationException("u can't instantiate me...");
    }

    /**
     * 判断对象是否为空
     *
     * @param object 被校验对象
     * @return boolean (true:为空；false:非空)
     */
    public static boolean isEmpty(Object object) {
        if (object == null) {
            return true;
        }
        if (object instanceof String && "".equals(object)) {
            return true;
        }
        if (object.getClass().isArray() && Array.getLength(object) == 0) {
            return true;
        }
        if (object instanceof Collection && ((Collection) object).isEmpty()) {
            return true;
        }
        return object instanceof Map && ((Map) object).isEmpty();
    }

    /**
     * 判断对象是否非空
     *
     * @param object 被校验对象
     * @return boolean (true:非空；false:为空)
     */
    public static boolean isNotEmpty(Object object) {
        return !isEmpty(object);
    }
}
