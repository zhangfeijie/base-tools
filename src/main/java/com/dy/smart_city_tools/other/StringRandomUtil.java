package com.dy.smart_city_tools.other;

import java.util.Random;

/**
 * StringRandomUtil
 *
 * @author 黑小虎（花名）
 * @date 2020/1/8 11:18
 */
public class StringRandomUtil {
    /**
     * 生成用户名要用的基础字符数组
     */
    public static final char[] BASE_STRING = {'0','1','2','3','4','5','6','7','8','9',
            'a','b','c','d','e','f','g','h','i','j','k','l','m','n','o','p','q','r','s','t','u','v','w','x','y','z',
            'A','B','C','D','E','F','G','H','I','J','K','L','M','N','O','P','Q','R','S','T','U','V','W','X','Y','Z'};

    /**
     * 随机生成用户名
     *
     * @return String（例如：dyid_Qg6pVbcnM0K5mV1）
     */
    public static String createUsername() {
        Random random = new Random();
        StringBuilder sb = new StringBuilder("dyid_");
        for (int i = 0; i < 15; i++) {
            int number = random.nextInt(62);
            sb.append(BASE_STRING[number]);
        }
        return sb.toString();
    }
}
