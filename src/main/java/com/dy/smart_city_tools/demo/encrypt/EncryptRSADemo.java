package com.dy.smart_city_tools.demo.encrypt;

import com.dy.smart_city_tools.encrypt.RSAEncrypt;

import java.util.Scanner;

/**
 * @description:
 * @author: 黑小虎（花名）
 * @date: 2019/12/23 23:17
 */
public class EncryptRSADemo {
    public static void main(String[] args) throws Exception {
        //设置公私钥对存放路径，可选，默认是工程目录
        //RSAEncrypt.setKeyPath("C:\\Users\\DYZHCS\\Desktop\\aaaa\\key");
        RSAEncrypt.setKeyPath("src\\main\\resources\\");
//        System.out.println("请输入明文：");
//        Scanner sca = new Scanner(System.in);
//        String str = sca.nextLine();
//        System.out.println("============================");
//        String secret = RSAEncrypt.encryptWithRSA(str);
//        System.out.println("经过RSA加密后的密文为：");
//        System.out.println(secret);
//        System.out.println("============================");
//        String original = RSAEncrypt.decryptWithRSA(secret);
//        System.out.println("经过RSA解密后的原文为：");
//        System.out.println(original);
        StringBuilder secret = new StringBuilder()
                .append("JM8xbc/dgJtjgZ0wqu4ArNIhJOCv0Z9O16bXUw1mDvEXSQe40fmmnMoI6D8wEIDWPx0JuzQDM1L/")
                .append("us6eabZ9c5/Cmz/ubIbP/NDvu7EZlpMO34Mg2VDVUcEAh1FrUrijAxN9tGutcZioB80AQ8zQgZJp")
                .append("+t/1Izrz+yUcgP4/r/0=");
        System.out.println(secret);
        System.out.println("============================");
        String original = RSAEncrypt.decryptWithRSA(secret.toString());
        System.out.println("经过RSA解密后的原文为：");
        System.out.println(original);
    }
}
