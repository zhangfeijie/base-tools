package com.dy.smart_city_tools.demo.snowflake;

import com.dy.smart_city_tools.other.SnowflakeDYSC;

import java.util.concurrent.TimeUnit;

public class SnowFlakeRun {
    public static void main(String[] args) throws InterruptedException {
        //    snowflake:
        //      #取值范围：0 ~ 511
        //      datacenterId: 101
        //      #取值范围：0 ~ 7
        //      workerId: 1
        long datacenterId = 101;
        long workerId = 1;

        System.out.println(datacenterId);
        System.out.println(workerId);

        SnowflakeDYSC snowflake = new SnowflakeDYSC(datacenterId, workerId);

        long st = snowflake.timestampGen();
        System.out.println(st);

        while (true) {
            System.out.println(snowflake.nextId());
            TimeUnit.SECONDS.sleep(1);
        }
    }
}
