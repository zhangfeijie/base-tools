package com.dy.smart_city_tools.demo.jiaofei100;

import lombok.Data;
import org.springframework.stereotype.Component;
import org.springframework.util.DigestUtils;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.net.URL;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.Random;

public class JiaofeiRecharge {
    public static void main(String[] args) {
        DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyyMMddHHmmss");
        StringBuilder yyyyMMddHHmmss = new StringBuilder(formatter.format(LocalDateTime.now()));
        System.out.println("交易时间：" + yyyyMMddHHmmss);
        StringBuilder orderId = new StringBuilder(yyyyMMddHHmmss);
        Random random = new Random();
        for (int i = 0; i < 12; i++) {
            orderId.append(random.nextInt(10));
        }
        JiaofeiRequest jiaofeiRequest = new JiaofeiRequest();
        jiaofeiRequest.setTradeType("10");
        jiaofeiRequest.setOrderId(orderId);
        jiaofeiRequest.setAccount("17639601024");
        jiaofeiRequest.setBuyNum(1);
        jiaofeiRequest.setUnitPrice(30000 * 1000);
        jiaofeiRequest.setTotalPrice(30000 * 1000);
        jiaofeiRequest.setCreateTime(yyyyMMddHHmmss);
        jiaofeiRequest.setIsCallBack((byte) 0);
        //组合MD5格式参数
        StringBuilder md5 = new StringBuilder()
                .append("APIID=").append(JiaofeiRequest.APIID).append("&")
                .append("Account=").append(jiaofeiRequest.getAccount()).append("&")
                .append("BuyNum=").append(jiaofeiRequest.getBuyNum()).append("&")
                .append("CreateTime=").append(jiaofeiRequest.getCreateTime()).append("&")
                .append("isCallBack=").append(jiaofeiRequest.getIsCallBack()).append("&")
                .append("OrderID=").append(jiaofeiRequest.getOrderId()).append("&")
                .append("TotalPrice=").append(jiaofeiRequest.getTotalPrice()).append("&")
                .append("TradeType=").append(jiaofeiRequest.getTradeType()).append("&")
                .append("UnitPrice=").append(jiaofeiRequest.getUnitPrice()).append("&")
                .append("APIKEY=").append(JiaofeiRequest.APIKEY);
        System.out.println("MD5加密前：" + md5);
        //设置 -> 签名字符串
        String sign = DigestUtils.md5DigestAsHex(md5.toString().getBytes());
        System.out.println("MD5加密后：" + sign.toUpperCase());
        jiaofeiRequest.setSign(sign.toUpperCase());
        //-------------------------------------------
        String url = "http://open.jiaofei100.com/Api/PayMobile.aspx";
        System.out.println("请求链接：" + url);
        StringBuilder params = new StringBuilder("?")
                .append("APIID=").append(JiaofeiRequest.APIID).append("&")
                .append("TradeType=").append(jiaofeiRequest.getTradeType()).append("&")
                .append("Account=").append(jiaofeiRequest.getAccount()).append("&")
                .append("UnitPrice=").append(jiaofeiRequest.getUnitPrice()).append("&")
                .append("BuyNum=").append(jiaofeiRequest.getBuyNum()).append("&")
                .append("TotalPrice=").append(jiaofeiRequest.getTotalPrice()).append("&")
                .append("OrderID=").append(jiaofeiRequest.getOrderId()).append("&")
                .append("CreateTime=").append(jiaofeiRequest.getCreateTime()).append("&")
                .append("IsCallBack=").append(jiaofeiRequest.getIsCallBack()).append("&")
                .append("Sign=").append(jiaofeiRequest.getSign());
        System.out.println("请求参数：" + params);
        try {
            URL urlAll = new URL(url + params);
            InputStream is = urlAll.openConnection().getInputStream();
            ByteArrayOutputStream os = new ByteArrayOutputStream();
            byte[] b = new byte[1024]; int len;
            while ((len = is.read(b)) != -1) os.write(b, 0, len);
            String respBody = os.toString("UTF-8");
            os.close(); is.close();
            System.out.println(respBody);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}

@Component
@Data
class JiaofeiRequest {
    //合作用户系统编号
    public final static String APIID = "19110805968148";
    //合作用户系统密钥
    public final static String APIKEY = "6EAB01FF50747BE6EB2AC6B2717E21D8";
//    public final static String APIKEY = "6EAB01FF50747BE6EB2AC6B2717E21D9";

    //商品类型
    private String tradeType;
    //订单编号
    private StringBuilder orderId;
    //充值号码
    private String account;
    //购买数量
    private Integer buyNum;
    //商品面值（单位：厘）
    private Integer unitPrice;
    //订单总金额（面值*数量）
    private Integer totalPrice;
    //交易时间（格式：yyyyMMddHHmmss）
    private StringBuilder createTime;
    //是否开启异步通知（0:不开启；1:开启）
    private Byte isCallBack;

    /* 签名字符串
     * Md5(APIID=1504102**91864&
     *     Account=13896193383&
     *     BuyNum=1&
     *     CreateTime=20150426230043&
     *     isCallBack=0&
     *     OrderID=12345678910&
     *     TotalPrice=10000&
     *     TradeType=10&
     *     UnitPrice=10000&
     *     APIKEY=BA336*******B90568D) 转大写
     */
    private String sign;
}
