package com.dy.smart_city_tools.demo.redis;

import org.apache.commons.pool2.impl.GenericObjectPoolConfig;
import redis.clients.jedis.HostAndPort;
import redis.clients.jedis.JedisPool;

import java.util.HashSet;
import java.util.Map;
import java.util.ResourceBundle;
import java.util.Set;

public class JedisCluster {
    // 从src/main/resources/nosql/redis/config.propreties文件中读取配置信息
    private void test1() {
        ResourceBundle rb = ResourceBundle.getBundle("nosql/redis/config");
        String host = rb.getString("host");
        System.out.println(host);
    }
    
    public static void main(String[] args) {
        String message = "123456";

        // 开发环境集群实例节点列表
        Set<HostAndPort> nodes = new HashSet<>();
        nodes.add(new HostAndPort("192.168.130.133", 7001));
        nodes.add(new HostAndPort("192.168.130.133", 7002));
        nodes.add(new HostAndPort("192.168.130.133", 7003));
        nodes.add(new HostAndPort("192.168.130.133", 7004));
        nodes.add(new HostAndPort("192.168.130.133", 7005));
        nodes.add(new HostAndPort("192.168.130.133", 7006));

        // 测试环境集群实例节点列表
//        nodes.add(new HostAndPort("192.168.130.36", 6379));
//        nodes.add(new HostAndPort("192.168.130.37", 6379));
//        nodes.add(new HostAndPort("192.168.130.38", 6379));
//        nodes.add(new HostAndPort("192.168.130.39", 6379));
//        nodes.add(new HostAndPort("192.168.130.40", 6379));
//        nodes.add(new HostAndPort("192.168.130.41", 6379));

        GenericObjectPoolConfig config = new GenericObjectPoolConfig();
        config.setMaxTotal(100);
        config.setMaxIdle(10);
        config.setMaxWaitMillis(3000);
        config.setTestOnBorrow(true);

        redis.clients.jedis.JedisCluster jedisCluster = new redis.clients.jedis.JedisCluster(nodes, 3000, 3000, 3, "123456", config);
        jedisCluster.set("aaaaaa:" + message, message);

        String get = jedisCluster.get("aaaaaa:" + message);
        System.out.println(get);

        Map<String, JedisPool> clusterNodes = jedisCluster.getClusterNodes();
        Set<String> keySet = clusterNodes.keySet();
        for (String key : keySet) {
            JedisPool jedisPool = clusterNodes.get(key);
            System.out.println(key + " -> " + jedisPool);
        }
        jedisCluster.close();
    }
}
