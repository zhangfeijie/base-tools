package com.dy.smart_city_tools.request;

import org.springframework.stereotype.Component;

import java.io.Serializable;

/**
 * 基础请求参数的封装类
 *
 * @author 黑小虎（花名）
 * @date 2019/10/10 10:10
 */
@Component
public class InputObject implements Serializable {
    /**
     * APP：手机端 H5: h5端
     */
    public String terminal;
    /**
     * 请求时间(yyyy-MM-dd HH:mm:ss)
     */
    public String reqTime;
    /**
     * APP版本号
     */
    public String clientVersion;
    /**
     * 接口版本号
     */
    public String version;
    /**
     * token
     */
    public String token;
    /**
     * 加密数据
     */
    public String custId;
    /**
     * 终端类型 ios，android
     */
    public String terminalType;
    /**
     * 设备唯一标识
     */
    public String deviceIdfaImei;
    /**
     * 极光设备ID
     */
    public String jgRegId;

    public Object data;

    public Integer pageNum = 1;
    public Integer pageSize = 10;

    public String getTerminal() {
        return terminal;
    }

    public void setTerminal(String terminal) {
        this.terminal = terminal;
    }

    public String getReqTime() {
        return reqTime;
    }

    public void setReqTime(String reqTime) {
        this.reqTime = reqTime;
    }

    public String getClientVersion() {
        return clientVersion;
    }

    public void setClientVersion(String clientVersion) {
        this.clientVersion = clientVersion;
    }

    public String getVersion() {
        return version;
    }

    public void setVersion(String version) {
        this.version = version;
    }

    public String getToken() {
        return token;
    }

    public void setToken(String token) {
        this.token = token;
    }

    public String getCustId() {
        return custId;
    }

    public void setCustId(String custId) {
        this.custId = custId;
    }

    public String getTerminalType() {
        return terminalType;
    }

    public void setTerminalType(String terminalType) {
        this.terminalType = terminalType;
    }

    public String getDeviceIdfaImei() {
        return deviceIdfaImei;
    }

    public void setDeviceIdfaImei(String deviceIdfaImei) {
        this.deviceIdfaImei = deviceIdfaImei;
    }

    public String getJgRegId() {
        return jgRegId;
    }

    public void setJgRegId(String jgRegId) {
        this.jgRegId = jgRegId;
    }

    public Object getData() {
        return data;
    }

    public void setData(Object data) {
        this.data = data;
    }

    public Integer getPageNum() {
        return pageNum;
    }

    public void setPageNum(Integer pageNum) {
        this.pageNum = pageNum;
    }

    public Integer getPageSize() {
        return pageSize;
    }

    public void setPageSize(Integer pageSize) {
        this.pageSize = pageSize;
    }
}
