package com.dy.smart_city_tools.enums;

/**
 * ServiceModuleEnum
 *
 * @author 黑小虎（花名）
 * @date 2019/12/29 15:44
 */
public enum ServiceModuleEnum {
    /**
     * 以下是基础服务模块
     */
    BASE_SERVICE_PUSH("201", "push", "推送服务"),
    BASE_SERVICE_SEND("202", "send", "发送服务"),
    BASE_SERVICE_AUTH("203", "auth", "认证中心"),
    BASE_SERVICE_PAYMENT("204", "payment", "支付系统"),
    BASE_SERVICE_CREDIT("205", "credit", "信用通"),
    BASE_SERVICE_IM("206", "im", "即时通信"),
    BASE_SERVICE_CUSTOMER("207", "customer", "客服服务"),

    /**
     * 以下是业务服务模块
     */
    WORK_SERVICE_RECHARGE("301", "recharge", "充值中心"),
    WORK_SERVICE_CARDBAG("302", "cardbag", "卡包");

    /**
     * 服务模块编号
     */
    private final String code;
    /**
     * 服务模块标识
     */
    private final String sign;
    /**
     * 服务模块名称
     */
    private final String name;

    public String getCode() {
        return code;
    }

    public String getSign() {
        return sign;
    }

    public String getName() {
        return name;
    }

    ServiceModuleEnum(String code, String sign, String name) {
        this.code = code;
        this.sign = sign;
        this.name = name;
    }
}
