package com.dy.smart_city_tools.response;

import java.io.Serializable;

/**
 * ResponseCodeEnum
 *
 * @author 黑小虎（花名）
 * @date 2019/12/29 15:44
 */
public enum ResponseCodeEnum implements Serializable {
    /**
     * 基础响应状态码
     */
    SUCCESS("200", "成功", "ResponseCodeEnum: SUCCESS"),
    success("20000", "成功", "ResponseCodeEnum: SUCCESS"),
    FAILED("400", "失败", "ResponseCodeEnum: FAILED"),
    failed("40000", "失败", "ResponseCodeEnum: FAILED"),
    ERROR_SERVER("500", "服务器内部错误", "ResponseCodeEnum: ERROR_SERVER"),
    error_server("50000", "服务器内部错误", "ResponseCodeEnum: error_server"),

    ERROR_PARAM("10000", "接口参数错误", "ResponseCodeEnum: ERROR_PARAM"),
    ERROR_PHONE("10001", "手机号码错误", "ResponseCodeEnum: ERROR_PHONE"),
    ERROR_TOKEN("10002", "登录认证错误", "ResponseCodeEnum: ERROR_TOKEN"), UNAUTHORIZED("401", "未签证", "ResponseCodeEnum: UNAUTHORIZED"),
    ERROR_USERID("10003", "用户ID错误", "ResponseCodeEnum: ERROR_USERID"),
    ERROR_USERNAME("10004", "用户名错误", "ResponseCodeEnum: ERROR_USERNAME"),
    ERROR_PASSWORD_LOGIN("10005", "登录密码错误", "ResponseCodeEnum: ERROR_PASSWORD_LOGIN"),
    ERROR_PASSWORD_PAYMENT("10006", "支付密码错误", "ResponseCodeEnum: ERROR_PASSWORD_PAYMENT"),
    ERROR_CODE_SMS("10007", "短信验证码错误", "ResponseCodeEnum: ERROR_CODE_SMS"),
    ERROR_CODE_JPG("10008", "图片验证码错误", "ResponseCodeEnum: ERROR_CODE_JPG"),
    ERROR_CODE_SLIDE("10009", "滑块验证码错误", "ResponseCodeEnum: ERROR_CODE_SLIDE"),
    ERROR_ACCESS("10010", "非法访问", "ResponseCodeEnum: ERROR_ACCESS"),
    ERROR_TERMINAL("10011", "应用类型错误", "ResponseCodeEnum: ERROR_TERMINAL"),
    ERROR_TERMINALTYPE("10012", "终端类型错误", "ResponseCodeEnum: ERROR_TERMINALTYPE"),
    ERROR_CODE_EMAIL("10013", "邮箱验证码错误", "ResponseCodeEnum: ERROR_CODE_EMAIL"),

    LALALA("", "", "");

    /**
     * 状态码编号
     */
    private final String code;
    /**
     * 状态码说明（用作前端提示）
     */
    private final String respMesg;
    /**
     * 状态码说明（用作后台日志）
     */
    private final String backMesg;

    public String getCode() {
        return code;
    }

    public String getRespMesg() {
        return respMesg;
    }

    public String getBackMesg() {
        return backMesg;
    }

    ResponseCodeEnum(String code, String respMesg, String backMesg) {
        this.code = code;
        this.respMesg = respMesg;
        this.backMesg = backMesg;
    }
}
