package com.dy.smart_city_tools.response;

import org.springframework.stereotype.Component;

import java.io.Serializable;

/**
 * 响应数据基础格式
 *
 * @author 黑小虎（花名）
 * @date 2019/10/10 10:10
 */
@Component
public class OutputObject implements Serializable {
    public String respCode;
    public String respMessage;
    public Object data;

    public String getRespCode() {
        return respCode;
    }

    public OutputObject setRespCode(String respCode) {
        this.respCode = respCode;
        return this;
    }

    public String getRespMessage() {
        return respMessage;
    }

    public OutputObject setRespMessage(String respMessage) {
        this.respMessage = respMessage;
        return this;
    }

    public Object getData() {
        return data;
    }

    public OutputObject setData(Object data) {
        this.data = data;
        return this;
    }

    @Override
    public String toString() {
        return "OutputObject{" +
                "respCode=" + respCode +
                ", respMessage='" + respMessage + '\'' +
                ", data=" + data +
                '}';
    }
}
