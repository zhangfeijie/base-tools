package com.dy.smart_city_tools.response;

import lombok.Data;
import org.springframework.stereotype.Component;

import java.io.Serializable;
import java.util.Map;

/**
 * OutputObjectMap
 *
 * @author 黑小虎（花名）
 * @date 2020/1/14 11:13
 */
@Component
@Data
public class OutputObjectMap implements Serializable {
    private String respCode;
    private String respMessage;
    private Map<String, String> data;
}
