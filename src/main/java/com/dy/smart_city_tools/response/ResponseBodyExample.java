package com.dy.smart_city_tools.response;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

/**
 * 封装响应体的接口的默认实现类
 *
 * @author 黑小虎（花名）
 * @date 2019/10/10 10:10
 */
@Component
public class ResponseBodyExample implements ResponseBody {
    @Autowired
    private OutputObject outputObject;

    /**
     * 请求参数不满足接口的要求
     *
     * @param code
     * @param message
     * @return OutputObject
     */
    @Override
    public OutputObject isErrorParam(String code, String message) {
        outputObject.setRespCode(code == null ? ResponseCodeEnum.ERROR_PARAM.getCode() : code);
        outputObject.setRespMessage(message);
        return outputObject;
    }

    /**
     * 服务器出错了或运行时异常
     *
     * @param message
     * @param errorMessage
     * @return OutputObject
     */
    @Override
    public OutputObject isErrorServer(String message, String errorMessage) {
        outputObject.setRespCode(ResponseCodeEnum.ERROR_SERVER.getCode());
        outputObject.setRespMessage(message + "：出错");
        outputObject.setData(errorMessage);
        return outputObject;
    }

    /*
     * insert返回值：成功返回影响条数；失败就是出错了，报异常
     * delete返回值：成功返回影响条数；失败返回 0；错误报异常
     * update返回值：成功返回匹配条数；失败返回 0；错误报异常
     * select返回值：成功返回对象或List集合；失败返回 null；错误报异常
     */

    /**
     * 判断insert、delete、update操作是否成功
     *
     * @param message
     * @param status
     * @return OutputObject
     */
    @Override
    public OutputObject ifStatus(String message, Integer status) {
        if (status > 0) {
            outputObject.setRespCode(ResponseCodeEnum.SUCCESS.getCode());
            outputObject.setRespMessage(message + "：成功");
            outputObject.setData(true);
        } else {
            outputObject.setRespCode(ResponseCodeEnum.FAILED.getCode());
            outputObject.setRespMessage(message + "：失败");
            outputObject.setData(false);
        }
        return outputObject;
    }

    /**
     * 判断select操作结果并封装响应体
     *
     * @param message
     * @param result
     * @return OutputObject
     */
    @Override
    public OutputObject ifResult(String message, Object result) {
        outputObject.setRespCode(ResponseCodeEnum.SUCCESS.getCode());
        outputObject.setRespMessage(message);
        outputObject.setData(result);
        return outputObject;
    }
}
