package com.dy.smart_city_tools.response;

/**
 * 封装响应体的接口
 *
 * @author 黑小虎（花名）
 * @date 2019/10/10 10:10
 */
public interface ResponseBody {
    /**
     * 请求参数不满足接口的要求
     *
     * @param code
     * @param message
     * @return OutputObject
     */
    OutputObject isErrorParam(String code, String message);

    /**
     * 服务器出错了或运行时异常
     *
     * @param message
     * @param errorMessage
     * @return OutputObject
     */
    OutputObject isErrorServer(String message, String errorMessage);

    /*
     * insert返回值：成功返回影响条数；失败就是出错了，报异常
     * delete返回值：成功返回影响条数；失败返回 0；错误报异常
     * update返回值：成功返回匹配条数；失败返回 0；错误报异常
     * select返回值：成功返回对象或List集合；失败返回 null；错误报异常
     */

    /**
     * 判断insert、delete、update操作是否成功
     *
     * @param message
     * @param status
     * @return OutputObject
     */
    OutputObject ifStatus(String message, Integer status);

    /**
     * 判断select操作结果并封装响应体
     *
     * @param message
     * @param result
     * @return OutputObject
     */
    OutputObject ifResult(String message, Object result);
}
