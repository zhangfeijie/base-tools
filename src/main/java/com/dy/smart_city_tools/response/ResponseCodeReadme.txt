状态码分类前缀：
    IS_NULL_：不能为空
    NOT_EXIST_：不存在
    OVER_TIMES_：超过次数
    WRONGFUL_：不合法的


基础响应状态码定义：
    SUCCESS("200", "成功", "ResponseCodeEnum: SUCCESS"),
    success("20000", "成功", "ResponseCodeEnum: SUCCESS"),
    FAILED("400", "失败", "ResponseCodeEnum: FAILED"),
    failed("40000", "失败", "ResponseCodeEnum: FAILED"),
    ERROR_SERVER("500", "服务器内部错误", "ResponseCodeEnum: ERROR_SERVER"),
    error_server("50000", "服务器内部错误", "ResponseCodeEnum: error_server"),

    ERROR_PARAM("10000", "接口参数错误", "ResponseCodeEnum: ERROR_PARAM"),
    ERROR_PHONE("10001", "手机号码错误", "ResponseCodeEnum: ERROR_PHONE"),
    ERROR_TOKEN("10002", "登录认证错误", "ResponseCodeEnum: ERROR_TOKEN"), UNAUTHORIZED("401", "未签证", "ResponseCodeEnum: UNAUTHORIZED"),
    ERROR_USERID("10003", "用户ID错误", "ResponseCodeEnum: ERROR_USERID"),
    ERROR_USERUSER("10004", "用户名错误", "ResponseCodeEnum: ERROR_USERUSER"),
    ERROR_PASSWORD_LOGIN("10005", "登录密码错误", "ResponseCodeEnum: ERROR_PASSWORD_LOGIN"),
    ERROR_PASSWORD_PAYMENT("10006", "支付密码错误", "ResponseCodeEnum: ERROR_PASSWORD_PAYMENT"),
    ERROR_CODE_SMS("10007", "短信验证码错误", "ResponseCodeEnum: ERROR_CODE_SMS"),
    ERROR_CODE_JPG("10008", "图片验证码错误", "ResponseCodeEnum: ERROR_CODE_JPG"),
    ERROR_CODE_SLIDE("10009", "滑块验证码错误", "ResponseCodeEnum: ERROR_CODE_SLIDE"),
    ERROR_ACCESS("10010", "非法访问", "ResponseCodeEnum: ERROR_ACCESS");

业务响应状态码定义：
    格式：前3位+后3位  ->  101001
    说明：前3位是模块编号（由此项目统一分配），后3位是各模块的业务编号（由各模块分配）
    模块编号分配列表：
        // 以下是基础模块
        推送服务：201
        发送服务：202
        认证中心：203
        支付系统：204
        信 用 通：205
        即时通信：206
        客服服务：207
        // 以下是业务模块
        充值中心：301
        卡    包：302




