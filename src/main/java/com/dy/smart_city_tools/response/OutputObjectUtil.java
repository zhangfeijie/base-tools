package com.dy.smart_city_tools.response;

/**
 * OutputObjectUtil
 *
 * @author 黑小虎（花名）
 * @date 2019年10月10日 10:10
 */
public class OutputObjectUtil {
    /**
     * 处理失败后的响应内容的封装：默认的
     *
     * @return OutputObject
     */
    public static OutputObject encapFailedResult() {
        return new OutputObject()
                .setRespCode(ResponseCodeEnum.FAILED.getCode())
                .setRespMessage(ResponseCodeEnum.FAILED.getRespMesg());
    }

    /**
     * 处理失败后的响应内容的封装：自定义说明
     *
     * @param message
     * @return OutputObject
     */
    public static OutputObject encapFailedResult(String message) {
        return new OutputObject()
                .setRespCode(ResponseCodeEnum.FAILED.getCode())
                .setRespMessage(message);
    }

    /**
     * 处理失败后的响应内容的封装：自定义状态码和说明
     *
     * @param code
     * @param message
     * @return OutputObject
     */
    public static OutputObject encapFailedResult(String code, String message) {
        return new OutputObject()
                .setRespCode(code)
                .setRespMessage(message);
    }

    /**
     * 处理失败后的响应内容的封装：自定义说明和结果
     *
     * @param message
     * @return OutputObject
     */
    public static OutputObject encapFailedResult(String message, Object data) {
        return new OutputObject()
                .setRespCode(ResponseCodeEnum.FAILED.getCode())
                .setRespMessage(message)
                .setData(data);
    }

    /**
     * 处理失败后的响应内容的封装：自定义状态码、说明以及结果
     *
     * @param code
     * @param message
     * @param data
     * @return OutputObject
     */
    public static OutputObject encapFailedResult(String code, String message, Object data) {
        return new OutputObject()
                .setRespCode(code)
                .setRespMessage(message)
                .setData(data);
    }

    // 处理失败后的响应内容的封装：自定义状态码、说明以及错误信息
    public static OutputObject encapFailedResult(String code, String message, String errorMessage) {
        return new OutputObject()
                .setRespCode(code)
                .setRespMessage(message)
                .setData(errorMessage);
    }

    /**
     * 以上是【处理失败】后封装响应内容的方法
     * ---------------------------------------------------------------
     * 以下是【处理成功】后封装响应内容的方法
     */

    /**
     * 处理成功后的响应内容的封装：默认的
     *
     * @return OutputObject
     */
    public static OutputObject encapSuccessResult() {
        return new OutputObject()
                .setRespCode(ResponseCodeEnum.SUCCESS.getCode())
                .setRespMessage(ResponseCodeEnum.SUCCESS.getBackMesg());
    }

    /**
     * 处理成功后的响应内容的封装：自定义说明
     *
     * @param message
     * @return OutputObject
     */
    public static OutputObject encapSuccessResult(String message) {
        return new OutputObject()
                .setRespCode(ResponseCodeEnum.SUCCESS.getCode())
                .setRespMessage(message);
    }

    /**
     * 处理成功后的响应内容的封装：自定义结果
     *
     * @param data
     * @return OutputObject
     */
    public static OutputObject encapSuccessResult(Object data) {
        return new OutputObject()
                .setRespCode(ResponseCodeEnum.SUCCESS.getCode())
                .setRespMessage(ResponseCodeEnum.SUCCESS.getRespMesg())
                .setData(data);
    }

    /**
     * 处理成功后的响应内容的封装：自定义结果和说明
     *
     * @param message
     * @param data
     * @return OutputObject
     */
    public static OutputObject encapSuccessResult(String message, Object data) {
        return new OutputObject()
                .setRespCode(ResponseCodeEnum.SUCCESS.getCode())
                .setRespMessage(message)
                .setData(data);
    }
}
